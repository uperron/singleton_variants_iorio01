import pandas as pd


annot_vars_data = pd.read_csv(snakemake.input.annot_vars_tab,
                              sep="\t", header=0)

# extract the "screening candidate" vars
# these are vars from essential genes for which there's a drug
# that haven't been screened in the GDSC
candidate_df = annot_vars_data[(annot_vars_data.essentiality_annot == True) &
                               (annot_vars_data.dep_ctype_matches_cell_ctype == True) &
                               (annot_vars_data["drug-gene_annot"] == True) &
                               (annot_vars_data.drug_cell_in_GDSC == False)].\
    sort_values("medFitEff").drop(
        ["CELL_LINE_NAME", "CELL_TCGA_DESC", "CELL_TCGA_PRJ", "DRUG_NAME_response", "SANGER_MODEL_ID",
         "IC50", "MAX_CONC", "IC50_ratio"], axis=1).drop_duplicates()

# load response data(IC50) from GDSC1 & GDSC2
GDSC_response_data = pd.concat([pd.read_csv(snakemake.input.gdsc1_tab),
                                pd.read_csv(snakemake.input.gdsc2_tab)]).drop_duplicates()
GDSC_response_data.COSMIC_ID = GDSC_response_data.COSMIC_ID.astype(
    int).astype(str)
all_GDSC_drugs = GDSC_response_data.DRUG_NAME.unique()
all_GDSC_lines = GDSC_response_data.COSMIC_ID.astype(int).astype(str).unique()


# check if drug in GDSC
candidate_df["drug_in_GDSC"] = candidate_df.DRUG_NAME_target.isin(
    all_GDSC_drugs)
candidate_df["COSMIC_ID"] = candidate_df.COSMIC_ID.astype(int).astype(str)

# check if cell line in GDSC
candidate_df["cellline_in_GDSC"] = candidate_df.COSMIC_ID.isin(all_GDSC_lines)
candidate_df = pd.merge(candidate_df,
                        GDSC_response_data[["COSMIC_ID", "CELL_LINE_NAME",
                                            "SANGER_MODEL_ID", "TCGA_DESC"]].drop_duplicates(),
                        on="COSMIC_ID",
                        how="left").drop_duplicates()

candidate_df = candidate_df[['hgvs_protein', 'hgvs', 'COSMIC_ID', 'GENE', 'Transcript',
                             'essentiality_annot', 'dep_ctype_matches_cell_ctype', 'vep_annot',
                             'CELL_LINE_NAME', 'SANGER_MODEL_ID', 'TCGA_DESC', 'dep_ctype', 'DRUG_NAME_target',
                             'drug-gene_annot', 'drug_cell_in_GDSC', 'drug_in_GDSC', 'cellline_in_GDSC',
                             'impact', 'consequence_terms', 'cadd_phred',
                             'polyphen_prediction', 'sift_prediction', 'medFitEff', 'rank_ratio',
                             "is_hotspot",
                              "isin_tcga_pan",
                              "var_freq_pan",
                              "freq_all_prj",
                              "isin_ctype_spec_prj"]]

# list of candidate var-drug-line  for which there's a
# 1) a GDSC drug targeting essential gene w/t var
# 2) a GDSC cell line with var and same ctype as essentiality analysis
# 3) cell line is not screened with drug
# these are "filling the matrix" candidates
candidate_inGDSC_df = candidate_df[(candidate_df.cellline_in_GDSC == True) &
                                   (candidate_df.drug_in_GDSC == True)].drop_duplicates()
candidate_inGDSC_df.to_csv(
    snakemake.output.candidates_GDSC_tab, sep="\t", index=None)

# list of candidate var-drug-line  for which there's a
# 1) a NON-GDSC drug targeting essential gene w/t var (Goncalvels et al 2020)
# 2) a GDSC cell line with var and same ctype as essentiality analysis
# 3) cell line is not screened with drug
# these are "filling the matrix" candidates
candidate_NOTinGDSC_df = candidate_df[(candidate_df.cellline_in_GDSC == True) &
                                      (candidate_df.drug_in_GDSC == False)].drop_duplicates()
candidate_NOTinGDSC_df.to_csv(
    snakemake.output.candidates_nonGDSC_tab, sep="\t", index=None)
