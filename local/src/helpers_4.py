# Functions to query Ensembl VEP all variant in the Cell Model Passports mutation annotation

import requests
import pandas as pd
import time


def get_vep_data(hgvs, reference=38):
    if reference == 37: 
        server =  "https://grch37.rest.ensembl.org/"
    if reference == 38:
        server = "https://rest.ensembl.org"
    ext = "/vep/human/hgvs/" + hgvs + "?"
    optional = "CADD=1&Conservation=1&domains=1&uniprot=1&variant_class=1&vcf_string=1"

    general_cols = ["id", "seq_region_name", "start", "end",
                    "strand", "variant_class", 
                    "most_severe_consequence", "vcf_string"]
    trasncript_cols = ["transcript_id", 'gene_symbol',
                       'uniparc', 'swissprot', 
                       'protein_start', 'protein_end',
                       'domains', 'amino_acids', 'codons', 'variant_allele',
                       'biotype', 'impact', 'consequence_terms',
                       'cadd_raw', 'cadd_phred',
                       "polyphen_score", 'polyphen_prediction',
                       'sift_score', "sift_prediction"]
    all_cols = general_cols + trasncript_cols

    transcript_id = hgvs.split(":")[0]
    fail_output = pd.Series(["NA"]*len(all_cols), index=all_cols)
    fail_output["id"] = hgvs

    url = server+ext+optional
    try:
        r = requests.get(url,
                         headers={"Content-Type": "application/json"},
                         timeout=60)
    except:
        print(f"Timeout returned for {url}")
        return fail_output

    if r.status_code != 200:
        print(f"Code {r.status_code} returned for {url}")
        return fail_output

    time.sleep(3)  # play nice

    decoded = r.json()[0]  # only one set of results each query
    # get general info on SNV
    row = pd.Series([decoded.get(k, "NA")
                     for k in general_cols], index=general_cols)
    arr = []
    # get further info on affected transcripts
    for trascript_dict in decoded["transcript_consequences"]:
        # select one transcript from the hgvs notation (and its variants)
        if transcript_id == trascript_dict["transcript_id"]:
            # this is safe but slower
            s = pd.Series([trascript_dict.get(k, "NA")
                           for k in trasncript_cols], index=trasncript_cols)
            # get Pfam domain info
            if s["domains"] != "NA":
                s["domains"] = [d["name"]
                                for d in s["domains"] if d["db"] == "Pfam_domain"]
            # convert list columns to strings
            s[["uniparc",
               "consequence_terms",
               "swissprot",
               "domains"]] = s[["uniparc",
                                "consequence_terms",
                                "swissprot",
                                "domains"]].apply(lambda x: (";").join(x))
            arr.append(row.append(s))
    print(f"{hgvs} done")
    return pd.concat(arr)
