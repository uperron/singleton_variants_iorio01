
######## Snakemake header ########
import sys; sys.path.insert(0, "/home/umberto.perron/anaconda3/envs/singleton_variants/lib/python3.6/site-packages"); import pickle; snakemake = pickle.loads(b'\x80\x03csnakemake.script\nSnakemake\nq\x00)\x81q\x01}q\x02(X\x05\x00\x00\x00inputq\x03csnakemake.io\nInputFiles\nq\x04)\x81q\x05(X\x1e\x00\x00\x00dependent_hgvs_clinrel_pre.tsvq\x06X7\x00\x00\x00cBioPortal_ALL_tcga_pan_can_atlas_2018_GRCh37_mutationsq\x07e}q\x08X\x06\x00\x00\x00_namesq\t}q\nsbX\x06\x00\x00\x00outputq\x0bcsnakemake.io\nOutputFiles\nq\x0c)\x81q\rX\x1d\x00\x00\x00ENST00000375147_clinrel_annotq\x0ea}q\x0f(h\t}q\x10X\x0b\x00\x00\x00clinrel_tabq\x11K\x00N\x86q\x12sh\x11h\x0eubX\x06\x00\x00\x00paramsq\x13csnakemake.io\nParams\nq\x14)\x81q\x15}q\x16h\t}q\x17sbX\t\x00\x00\x00wildcardsq\x18csnakemake.io\nWildcards\nq\x19)\x81q\x1aX\x0f\x00\x00\x00ENST00000375147q\x1ba}q\x1c(h\t}q\x1dX\x07\x00\x00\x00enst_idq\x1eK\x00N\x86q\x1fsX\x07\x00\x00\x00enst_idq h\x1bubX\x07\x00\x00\x00threadsq!K\x08X\t\x00\x00\x00resourcesq"csnakemake.io\nResources\nq#)\x81q$(K\x08K\x01e}q%(h\t}q&(X\x06\x00\x00\x00_coresq\'K\x00N\x86q(X\x06\x00\x00\x00_nodesq)K\x01N\x86q*uh\'K\x08h)K\x01ubX\x03\x00\x00\x00logq+csnakemake.io\nLog\nq,)\x81q-}q.h\t}q/sbX\x06\x00\x00\x00configq0}q1(X\x0c\x00\x00\x00BIOINFO_ROOTq2X0\x00\x00\x00/home/umberto.perron@htechnopole.local/snaketreeq3X\x08\x00\x00\x00PRJ_ROOTq4XG\x00\x00\x00/home/umberto.perron@htechnopole.local/snaketree/prj/Singleton_Variantsq5X\x07\x00\x00\x00VERSIONq6X\x01\x00\x00\x002q7X\x08\x00\x00\x00DATA_DIRq8X\x17\x00\x00\x00../../local/share/data/q9X\x10\x00\x00\x00ALL_VARIANTS_TABq:X#\x00\x00\x00../../local/share/data/TableS2C.csvq;X\x0f\x00\x00\x00DRUG_TARGET_TABq<X[\x00\x00\x00../../local/share/data/Gon\xc3\xa7alves_2020_suppmat/msb199405-sup-0003-datasetev2_drug_sheet.csvq=X\t\x00\x00\x00GDSC1_TABq>X=\x00\x00\x00../../local/share/data/GDSC1_fitted_dose_response_25Feb20.csvq?X\t\x00\x00\x00GDSC2_TABq@X=\x00\x00\x00../../local/share/data/GDSC2_fitted_dose_response_25Feb20.csvqAX\x07\x00\x00\x00N_CORESqBK\x08X\r\x00\x00\x00MEDFITEFF_MAXqCX\x03\x00\x00\x00-.5qDX\x0e\x00\x00\x00RANK_RATIO_MAXqEG?\xf3333333uX\x04\x00\x00\x00ruleqFX\r\x00\x00\x00query_clinrelqGub.')
######## Original script #########
#!/usr/bin/env python
import pandas as pd
import numpy as np

# Options for pandas
# No warnings about setting value on copy of slice
pd.options.mode.chained_assignment = None
pd.options.display.max_columns = 600
pd.options.display.max_rows = 30

# setup connection to genome nexus API
from bravado.client import SwaggerClient
from pprint import pprint
gn = SwaggerClient.from_url('https://www.genomenexus.org/v2/api-docs',
                            config={"validate_requests":False,
                                    "validate_responses":False,
                                    "validate_swagger_spec":False})
# to split input into chunks
enst_id = snakemake.wildcards.enst_id
variant_data = pd.read_csv(snakemake.input[0], sep="\t", header=0)
variant_data = variant_data[variant_data.Transcript == enst_id]

tcga_all_prj_mut_df = pd.read_csv(snakemake.input[1],  sep="\t", header=0)

# get cancerhotspots annot from genome nexus
# is var a statistically significant mut hotspot?
# see https://www.cancerhotspots.org      DOI: 10.1158/2159-8290.CD-17-0321 
def get_hotspot_annot(var):
    c,s,e,r,v = var.split(",")
    alt_var = ",".join([c,s,e,v,r])
    try:
        # see: https://github.com/mskcc/cbsp-hackathon/blob/master/0-introduction/cbsp_hackathon.ipynb
        result = gn.annotation_controller.fetchVariantAnnotationByGenomicLocationGET(
            genomicLocation=var, 
            # adds extra annotation resources, not included in default response:
            fields='hotspots annotation_summary'.split()
        ).result()
        print(f"{var} annotated.")
        return result.hotspots
    except:
        try:
            # try to flip ref/var nucleotide
            result = gn.annotation_controller.fetchVariantAnnotationByGenomicLocationGET(
                genomicLocation=alt_var, 
                fields='hotspots mutation_assessor annotation_summary'.split()
            ).result()
            print(f"{var} annotated (flipped).")
            return result.hotspots
        except:
            print(f"{var} not found.")
            return None
    
# add hotspot annotation    
variant_data["hotspot_annot"] = variant_data.query_str.apply(get_hotspot_annot)

# add bool col for fitering
def is_hotspot(x):
    if x:
        return sum([len(a) for a in x.annotation]) > 0
    else:
        return False

variant_data["is_hotspot"] = variant_data["hotspot_annot"].apply(is_hotspot)

tot_patients_pan = tcga_all_prj_mut_df.uniquePatientKey.unique().shape[0]
tot_patients_spec = tcga_all_prj_mut_df.groupby("TCGA_PRJ").uniquePatientKey.nunique()

# is var in tcga pan cancer?
# in how many patients pan cancer / what freq?
# in which prjs?
# is var in prj that corresponds to cell ctype and dep analysis ctype?
# if so in how many patients / what freq?
def query_tcga_mut(var, cell_tcga_prj):
    out = [False, 0, 0, "", {}, False, 0, 0]
    try:
        c, s, e, r, v = str(var).split(",")
    except ValueError:
        # isin_tcga_pan", "n_patients_pan", "var_freq_pan",
        # "prjs", "freq_all_prj", "isin_ctype_spec_prj", "n_patients_var_spec", "var_freq_spec":
        return out
    result = tcga_all_prj_mut_df.query(
        'chr==@c & startPosition==@s & endPosition==@e &\
        referenceAllele==@r & variantAllele ==@v')
    # try to flip reference & variant (other strand?)
    if len(result) == 0:
        result = tcga_all_prj_mut_df.query(
            'chr==@c & startPosition==@s & endPosition==@e &\
        referenceAllele==@v & variantAllele ==@r')
    # var is not in tcga
    if len(result) == 0:
        return out
    # compute output
    else:
        isin_tcga_pan = True
        n_patients_pan = result.uniquePatientKey.unique().shape[0]
        freq_pan = n_patients_pan / tot_patients_pan
        prjs = ",".join(result.TCGA_PRJ.unique())
        freq_all_prj = {}
        for prj in result.TCGA_PRJ.unique().tolist():
            n_patients_spec = result[result.TCGA_PRJ == prj].uniquePatientKey.unique().shape[0]
            freq_all_prj[prj] = n_patients_spec / tot_patients_spec[prj]
        is_ctype_spec = cell_tcga_prj in list(freq_all_prj.keys())
        if is_ctype_spec:
            freq_spec = freq_all_prj[cell_tcga_prj]
            n_patients_spec = result[result.TCGA_PRJ == cell_tcga_prj].uniquePatientKey.unique().shape[0]
        else:
            freq_spec = 0
            n_patients_spec = 0
        out = [isin_tcga_pan, n_patients_pan, freq_pan, prjs, freq_all_prj, 
               is_ctype_spec, n_patients_spec, freq_spec]
    return out

arr = []
for var, cell_tcga_prj in zip(variant_data['query_str'].values,  
                              variant_data['CELL_TCGA_PRJ'].values):
 arr.append(query_tcga_mut(var, cell_tcga_prj))
tcga_annot = pd.DataFrame(arr, columns=["isin_tcga_pan", "n_patients_pan", "var_freq_pan",
                      "prjs", "freq_all_prj", "isin_ctype_spec_prj", "n_patients_var_spec", "var_freq_spec"])

out_df = pd.concat([variant_data, tcga_annot], axis=1)
out_df.to_csv(snakemake.output[0], sep="\t", index=None)
