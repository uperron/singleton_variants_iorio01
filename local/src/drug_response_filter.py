#!/usr/bin/env python
import pandas as pd
import math
# load annotated vars
dependency_vep_target_df = pd.read_csv(
    snakemake.input.dep_vep_target_tab, sep="\t", header=0)

# load response data(IC50) from GDSC1 & GDSC2
GDSC_response_data = pd.concat([pd.read_csv(snakemake.input.gdsc1_tab),
                                pd.read_csv(snakemake.input.gdsc2_tab)]).drop_duplicates()
GDSC_response_data = GDSC_response_data.rename(
    columns={"DRUG_NAME": "DRUG_NAME_response",
             "TCGA_DESC": "CELL_TCGA_PRJ"})
GDSC_response_data.COSMIC_ID = GDSC_response_data.COSMIC_ID.astype(
    int).astype(str)
dependency_vep_target_df.COSMIC_ID = dependency_vep_target_df.COSMIC_ID.astype(
    int).astype(str)

TCGA_abbrv_data = pd.read_csv(
    snakemake.input.tcga_abbrv_tab, sep="\t", header=0)

# add drug response info
dependency_vep_target_response_df = pd.merge(dependency_vep_target_df, GDSC_response_data,
                                             left_on=["COSMIC_ID",
                                                      "DRUG_NAME_target"],
                                             right_on=["COSMIC_ID",
                                                       "DRUG_NAME_response"],
                                             how="left").drop_duplicates()
# add TCGA prj description
dependency_vep_target_response_df = pd.merge(dependency_vep_target_response_df, TCGA_abbrv_data,
                                             left_on="CELL_TCGA_PRJ",
                                             right_on="Study_Abbreviation",
                                             how="left")
dependency_vep_target_response_df = dependency_vep_target_response_df.rename(
    columns={"Study_Name": "CELL_TCGA_DESC"})

# check if cell_line;drug in GDSC
dependency_vep_target_response_df["drug_cell_in_GDSC"] = dependency_vep_target_response_df.DRUG_NAME_response.notna(
)
dependency_vep_target_response_df["IC50"] = dependency_vep_target_response_df["LN_IC50"].astype(float).\
    apply(lambda x: math.exp(x))
# check if IC50<max_c
dependency_vep_target_response_df["IC50<MAX_CONC"] = dependency_vep_target_response_df.IC50 < \
    dependency_vep_target_response_df.MAX_CONC
# compute the IC50/MAX_C ratio
dependency_vep_target_response_df["IC50_ratio"] = dependency_vep_target_response_df.IC50 / \
    dependency_vep_target_response_df.MAX_CONC

# add drug response analysis data
# these results are analogous to the dependency analysis
# i.e. a rank ratio and percentile are computed for a var in a cell line
# when treated woith a drug against all aother cell lines of the same lineage
# treated with the same drug
drug_response_analysis_data = pd.read_table(snakemake.input.dr_analysis_tab,
                                            sep="\t", header=0)
dr_df = drug_response_analysis_data[["hgvs_protein",
                                     "drug_name",
                                     "cell_line",
                                     "cosmic_id",
                                     "dr_rank_ratio",
                                     "dr_Zscore",
                                     "dr_perctl"]]
dr_df["cosmic_id"] = dr_df["cosmic_id"].astype(
    int).astype(str)
# this merge excludes (ofc) drugs that target the var gene
# but are not in GDSC
dependency_vep_target_response_df = pd.merge(dependency_vep_target_response_df,
                                             dr_df,
                                             left_on=["hgvs_protein",
                                                      "DRUG_NAME_response", "COSMIC_ID"],
                                             right_on=["hgvs_protein",
                                                       "drug_name", "cosmic_id"],
                                             how="left")
# pick the best (lowest dr_rank_ratio) drug for each variant across cell lines that share the same ctype/lineage
all_filters_df = dependency_vep_target_response_df[(dependency_vep_target_response_df.essentiality_annot == True) &
                                                   (dependency_vep_target_response_df.dep_ctype_matches_cell_ctype == True) &
                                                   (dependency_vep_target_response_df.vep_annot == True) &
                                                   (dependency_vep_target_response_df["drug-gene_annot"] == True) &
                                                   (dependency_vep_target_response_df.drug_cell_in_GDSC == True) &
                                                   (dependency_vep_target_response_df["IC50<MAX_CONC"] == True)].\
                                                       groupby(['hgvs', 'dep_ctype']).dr_rank_ratio.\
                                                       nsmallest(1).reset_index(level=[0,1], drop=True)
# eval on all_filters_df index which is a subset of the int64 index from dependency_vep_target_response_df
# after dropping the grouped MultiIndex
dependency_vep_target_response_df["best_drug_for_var-ctype"] = \
    [True if x in all_filters_df.index else False for x in dependency_vep_target_response_df.index]

# remove unnnecessary cols
dependency_vep_target_response_df = dependency_vep_target_response_df[[
    "hgvs_protein", "hgvs", "SANGER_MODEL_ID", "COSMIC_ID", "CELL_LINE_NAME",
    "GENE", "dep_ctype", "CELL_TCGA_DESC", "CELL_TCGA_PRJ", "DRUG_NAME_response",
    "Transcript", 'essentiality_annot', 'dep_ctype_matches_cell_ctype',
    'vep_annot', "drug-gene_annot",
    "drug_cell_in_GDSC",
    "IC50<MAX_CONC", "best_drug_for_var-ctype",
    'impact', 'consequence_terms',
    'cadd_phred', 'polyphen_prediction',
    'sift_prediction', 'medFitEff', 'rank_ratio',
    "DRUG_NAME_target", "IC50", "MAX_CONC", "IC50_ratio",
    "dr_rank_ratio", "dr_perctl", "dr_Zscore",
    "is_hotspot",
    "isin_tcga_pan",
    "n_patients_pan",
    "var_freq_pan",
    "prjs",
    "freq_all_prj",
    "isin_ctype_spec_prj",
    "n_patients_var_spec",
    "var_freq_spec"]].drop_duplicates()

dependency_vep_target_response_df.sort_values('dr_rank_ratio').\
    to_csv(snakemake.output.dep_vep_target_response_tab,  sep="\t", index=None)
hits_df = dependency_vep_target_response_df[(dependency_vep_target_response_df.essentiality_annot == True) &
                                            (dependency_vep_target_response_df.dep_ctype_matches_cell_ctype == True) &
                                            (dependency_vep_target_response_df.vep_annot == True) &
                                            (dependency_vep_target_response_df["drug-gene_annot"] == True) &
                                            (dependency_vep_target_response_df.drug_cell_in_GDSC == True) &
                                            (dependency_vep_target_response_df["IC50<MAX_CONC"] == True) &
                                            (dependency_vep_target_response_df["best_drug_for_var-ctype"] == True)].\
    sort_values('dr_rank_ratio')
hits_df.to_csv(snakemake.output.hits_tab, sep="\t", index=None)
