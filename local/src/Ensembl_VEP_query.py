# ---
# jupyter:
#   jupytext:
#     formats: ipynb,py
#     text_representation:
#       extension: .py
#       format_name: light
#       format_version: '1.4'
#       jupytext_version: 1.2.1
#   kernelspec:
#     display_name: singleton_variants
#     language: python
#     name: singleton_variants
# ---

# # Introduction
#
# Query Ensembl VEP with singleton SNV from Iorio et al. 2016 and analyse the resulting predictions.

# ### Imports
# Import libraries and write settings here.

# +
# Data manipulation
import requests, sys
import pandas as pd
import numpy as np
import time

# Options for pandas
# No warnings about setting value on copy of slice
pd.options.mode.chained_assignment = None
pd.options.display.max_columns = 600
pd.options.display.max_rows = 30

# Display all cell outputs
from IPython.core.interactiveshell import InteractiveShell
InteractiveShell.ast_node_interactivity = 'all'

from IPython import get_ipython
ipython = get_ipython()

# autoreload extension
if 'autoreload' not in ipython.extension_manager.loaded:
    %load_ext autoreload

# %autoreload 2

# Visualizations
import matplotlib.pyplot as plt
# Set default font size
plt.rcParams['font.size'] = 24
import seaborn as sb
# Set default font size
sb.set(font_scale = .8)
custom_style = {'axes.labelcolor': 'black',
                'xtick.color': 'black',
                'ytick.color': 'black'}
sb.set_style("white", rc=custom_style)

# -

# # Analysis/Modeling
# Do work here

# SNP Transcript identifiers in the table (https://www.cancerrxgene.org/gdsc1000/GDSC1000_WebResources//Data/suppData/TableS2C.xlsx) are from Ensembl version 56
# This corresponds to GRCh37 / Feb 2009 / hg19 (see ftp://ftp.ensembl.org/pub/release-56/gtf/homo_sapiens/)		
# We should use the grch37 version of Ensembl VEP to avoid issues w/t retired IDs which might not be not marked as retired.

filename = "data/TableS2C.csv"
variant_data = pd.read_csv(filename, sep=",", skiprows=20)
variant_data["hgvs"] = variant_data[['Transcript', 'cDNA']].agg(':'.join, axis=1).values
variant_data


# +
def url_response(url):
    try:
        r = requests.get(url=url)
    except:
        return None
    if r.status_code == 200:
        json_result = r.json()
        return json_result
    else:
        return None


# use the grch37 version of Ensembl VEP
# the batch hgvs query endpoint is not available on the grch37 version
server_url = "https://grch37.rest.ensembl.org/vep/human/hgvs/"
hgvs = "ENST00000263100:c.842A>G" + "?"
transcript_id = hgvs.split(":")[0]
optional = "CADD=1&Conservation=1&domains=1&uniprot=1&variant_class=1"


general_cols = ["id", "seq_region_name", "start", "end",
                "strand", "variant_class", "most_severe_consequence"]
trasncript_cols = ["transcript_id", 'gene_symbol',
                   'uniparc', 'swissprot',
                   'protein_start', 'protein_end',
                   'domains', 'amino_acids', 'codons', 'variant_allele',
                   'biotype', 'impact', 'consequence_terms',
                   'cadd_raw', 'cadd_phred',
                   "polyphen_score", 'polyphen_prediction',
                   'sift_score', "sift_prediction"]
all_cols = general_cols + trasncript_cols

def get_vep_data(hgvs):
    transcript_id = hgvs.split(":")[0]
    url = server_url+hgvs+"?"+optional #assemble query URL
    
    fail_output = pd.Series(["NA"]*len(all_cols), index=all_cols)
    fail_output["id"] = hgvs
    
    try:
        r = requests.get(url, headers={"Content-Type": "application/json"}, timeout=60)
    except:
        print(f"Timeout returned for {url}")
        return fail_output

    if r.status_code != 200:
        print(f"Code {r.status_code} returned for {url}")
        return fail_output
        
    time.sleep(5) # play nice

    decoded = r.json()[0] # only one set of results each query
    # get general info on SNV
    row = pd.Series([decoded.get(k, "NA") for k in general_cols], index=general_cols)
    arr = []
    # get further info on affected transcripts
    for trascript_dict in decoded["transcript_consequences"]:
        # select one transcript from the hgvs notation (and its variants)
        if transcript_id in trascript_dict["transcript_id"]:
            # this is safe but slower
            s = pd.Series([trascript_dict.get(k, "NA") for k in trasncript_cols], index=trasncript_cols)
            # get Pfam domain info 
            if s["domains"] != "NA":
                s["domains"] = [d["name"] for d in s["domains"] if d["db"] == "Pfam_domain"]
            # convert list columns to strings
            s[["uniparc",
               "consequence_terms",
               "swissprot",
               "domains"]] = s[["uniparc",
                                "consequence_terms",
                                "swissprot",
                                "domains"]].apply(lambda x: (";").join(x))
            arr.append(row.append(s))
    print(f"{hgvs} done")
    return pd.concat(arr, axis=1)


# -

arr = []
N = 1000
hgvs_list = variant_data["hgvs"].unique()[:N]
N = len(hgvs_list)
i = 0
for h in hgvs_list:
    arr.append(get_vep_data(h))
    i+=1
    p = i/N * 100
    print(f"{p}%")
vep_data = pd.concat(arr, axis=1).T
vep_data.to_csv(f"data/vep_data_{N}hgvs.tsv", sep="\t", index=None)

# # Results
# Show graphs and stats here

N = 1000
vep_data = pd.read_csv(f"data/vep_data_{N}hgvs.tsv", sep="\t", index_col=None)
vep_data

vep_data.describe()

vep_data.columns

vep_data.biotype.value_counts()

vep_data.impact.value_counts()

vep_data.consequence_terms.value_counts()

vep_data.polyphen_prediction.value_counts()

vep_data.sift_prediction.value_counts()

plot_df = vep_data[["impact",
                   "consequence_terms",
                   "polyphen_prediction",
                   "sift_prediction"]].dropna(how="all").astype(str)
plot_df.value_counts()

# # Conclusions and Next Steps
# Summarize findings here


