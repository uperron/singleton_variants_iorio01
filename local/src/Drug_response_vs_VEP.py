# ---
# jupyter:
#   jupytext:
#     formats: ipynb,py
#     text_representation:
#       extension: .py
#       format_name: light
#       format_version: '1.4'
#       jupytext_version: 1.2.1
#   kernelspec:
#     display_name: singleton_variants
#     language: python
#     name: singleton_variants
# ---

# # Introduction
# Investigate the relationship between SNV VEP and drug response in cancer cell lines

# ### Imports
# Import libraries and write settings here.

# +
# Data manipulation
import pandas as pd
import numpy as np

# Options for pandas
# No warnings about setting value on copy of slice
pd.options.mode.chained_assignment = None
pd.options.display.max_columns = 600
pd.options.display.max_rows = 30

# Display all cell outputs
from IPython.core.interactiveshell import InteractiveShell
InteractiveShell.ast_node_interactivity = 'all'

from IPython import get_ipython
ipython = get_ipython()

# autoreload extension
if 'autoreload' not in ipython.extension_manager.loaded:
    %load_ext autoreload

# %autoreload 2

# Visualizations
import matplotlib.pyplot as plt
# Set default font size
plt.rcParams['font.size'] = 24
import seaborn as sb
# Set default font size
sb.set(font_scale = .8)
custom_style = {'axes.labelcolor': 'black',
                'xtick.color': 'black',
                'ytick.color': 'black'}
sb.set_style("white", rc=custom_style)


# Interactive Visualizations
# import plotly.plotly as py
# import plotly.graph_objs as go
# from plotly.offline import iplot, init_notebook_mode
# init_notebook_mode(connected=True)

# import cufflinks as cf
# cf.go_offline(connected=True)
# icf.set_config_file(theme='white')
# -

# # Analysis/Modeling
# Do work here

# Drug response as IC50 data is available from https://www.cancerrxgene.org/downloads/bulk_download
# Specifically ftp://ftp.sanger.ac.uk/pub/project/cancerrxgene/releases/current_release/GDSC1_fitted_dose_response_25Feb20.xlsx & ftp://ftp.sanger.ac.uk/pub/project/cancerrxgene/releases/current_release/GDSC2_fitted_dose_response_25Feb20.xlsx
# These are the updated cancerXgene data
#
# And from https://www.cancerrxgene.org/gdsc1000/GDSC1000_WebResources///Data/suppData/TableS4B.xlsx as AUC data (these are from Iorio et al. 2016)

# +
# load data, slice columns of interest
filename = "data/TableS2C.csv"
variant_data = pd.read_csv(filename, sep=",", skiprows=20)[
    ['SAMPLE', "COSMIC_ID", 'Transcript', 'cDNA']]
variant_data["hgvs"] = variant_data[['Transcript', 'cDNA']].agg(
    ':'.join, axis=1).values

N = 1000
filename = f"data/vep_data_{N}hgvs.tsv"
vep_data = pd.read_csv(filename, sep="\t", index_col=None)[['id', 'impact', 'consequence_terms',
                                                           'cadd_raw', 'cadd_phred', 'polyphen_score',
                                                           'polyphen_prediction',
                                                           'sift_score', 'sift_prediction']]

filename = "data/TableS4B.csv"
response_data = pd.read_csv(filename, header=0, skiprows=4)
response_data.columns = ["Cell line cosmic identifiers",
                             "Sample Names"] + response_data.columns.tolist()[2:]

# +
# merge
variant_data.COSMIC_ID = variant_data.COSMIC_ID.astype(str)
vep_data.id = vep_data.id.astype(str)
response_data["Cell line cosmic identifiers"] = response_data["Cell line cosmic identifiers"].astype(
    str)
df1 = pd.merge(variant_data, vep_data, left_on="hgvs",
               right_on="id", how="inner")
df1.drop("id", axis=1, inplace=True)

vep_response_df = pd.merge(df1, response_data, left_on="COSMIC_ID",
               right_on="Cell line cosmic identifiers", how="inner")
filename = f"data/vep_response_data_{N}hgvs.tsv"
vep_response_df.to_csv(filename, sep="\t", index=None)
vep_response_df
# -

drug_names = vep_response_df.columns[15:]
drug_auc_df = vep_response_df[drug_names]
vep_response_df["best_AUC"] = drug_auc_df.max(axis=1)
vep_response_df["best_drug"] = drug_auc_df.idxmax(axis=1)
vep_response_df["AUC_std"] = drug_auc_df.std(axis=1)
vep_response_df

stats_df = vep_response_df.drop(drug_names, axis=1)
stats_df.describe()

stats_df.corr(method="spearman")

# # Results
# Show graphs and stats here



# # Conclusions and Next Steps
# Summarize findings here


