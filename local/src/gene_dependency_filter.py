import pandas as pd
from glob import glob

# parse all variants tab (Iorio et al. 2016)
variant_data = pd.read_csv(snakemake.input.all_vars_tab, sep=",", skiprows=20)
# build a unique transcript var identifier in hgvs format
variant_data["hgvs"] = variant_data[['Transcript', 'cDNA']].agg(
    ':'.join, axis=1).values

# load response data(IC50) from GDSC1 & GDSC2
GDSC_response_data = pd.concat([pd.read_csv(snakemake.input.gdsc1_tab),
                                pd.read_csv(snakemake.input.gdsc2_tab)]).drop_duplicates()
GDSC_response_data = GDSC_response_data.rename(
    columns={"DRUG_NAME": "DRUG_NAME_response",
             "TCGA_DESC": "CELL_TCGA_PRJ"})
GDSC_response_data = GDSC_response_data[[
    "COSMIC_ID", "CELL_TCGA_PRJ"]].drop_duplicates()
variant_data = pd.merge(variant_data, GDSC_response_data,
                        on="COSMIC_ID",
                        how="left")

# parse dependency data (FI's analysis)
filenames = glob(f'{snakemake.params.dep_data_dir}*.tsv')
dependency_df = pd.concat(
    [pd.read_csv(f, sep="\t", header=0) for f in filenames])
# cancer type where the dependency analysis is performed for this var
dependency_df = dependency_df.rename(columns={"ctype": "dep_ctype"})

# expand rows w/t multiple variants (e.g. var1|var2|var3)
# from now on these are considered independent vars
gb = dependency_df[["GENE", "var", "dep_ctype",
                    'medFitEff', 'rank_ratio']].groupby(["GENE",
                                                         "dep_ctype", 
                                                         'medFitEff', 
                                                         'rank_ratio'])  # group by all except var field
arr = []
for name, group in gb:
    GENE, dep_ctype, medFitEff, rank_ratio = name
    l = group["var"].astype(str).apply(
        lambda x: x.split(" | ")).values[0]  # extract list of vars
    for e in l:
        # expand row
        arr.append([GENE, dep_ctype, medFitEff, rank_ratio, e])
dependency_df = pd.DataFrame(
    arr, columns=["GENE", "dep_ctype", 'medFitEff', 'rank_ratio', "var"])
#  build protein var id
dependency_df["hgvs_protein"] = dependency_df[["GENE", "var"]].agg(
    ':'.join, axis=1).values
# map dep_type to TCGA abbrv
TCGA_abbrv_data = pd.read_csv(
    snakemake.input.tcga_abbrv_tab, sep="\t", header=0)
dependency_df = pd.merge(dependency_df, TCGA_abbrv_data,
                         left_on="dep_ctype",
                         right_on="Pip_right_ctype",
                         how="left")

# merge with full dataset of variants (non-sigletons as well)
dependency_df = pd.merge(variant_data, dependency_df, left_on=["Gene", "AA"],
                         right_on=["GENE", "var"], how="right")

# add bool col for gene essentiality filter
medFitEff_max = snakemake.params.medFitEff_max
rank_ratio_max = snakemake.params.rank_ratio_max
dependency_df["essentiality_annot"] = (dependency_df["medFitEff"] < medFitEff_max) &\
    (dependency_df["rank_ratio"] < rank_ratio_max)
# add bool col for cancer/tissue type match b/w dependency analysis and cell line
dependency_df["dep_ctype_matches_cell_ctype"] = dependency_df.Study_Abbreviation == dependency_df.CELL_TCGA_PRJ

# dependent-only vars for vep/clinrel annotation
dependency_df[dependency_df["essentiality_annot"] == True].to_csv(
    snakemake.output.dep_vars_tab, sep="\t", index=None)
# all singleton vars for further annotation
dependency_df.to_csv(snakemake.output.sngltn_vars_tab, sep="\t", index=None)
