#!/usr/bin/env python
import pandas as pd

# Options for pandas
# No warnings about setting value on copy of slice
pd.options.mode.chained_assignment = None

colnames=["screen",
        "drug_id",
        "drug_name",
        "drug_target",
        "min_conc",
        "max_conc",
        "dr_rank_ratio",
        "lnIC50",
        "dr_Zscore",
        "dr_perctl",
        "concRatio"]

# reshape the drug response analysis output
def reshape_df(filename):
    arr = []
    # extract gene name, variant (grouped), tissue type from filename
    gene_name = filename.split("/")[-1].split()[0]
    var = " | ".join([s.replace("_screenRes.tsv", "").replace(f"{gene_name} _ ***", "").strip() for s in filename.\
        split("/")[-1].split(" | ") if "p." in s])
    ctype = filename.split("/")[-2]
    # read analysis output table
    df = pd.read_csv(filename, sep="\t").dropna(how="any").reset_index(drop=True)
    lines = [c.replace("celllines.", "") for c in df.columns if "celllines" in c]
    # if only one cell line is included
    if lines == []:
        df.columns=colnames + ["cell_line", "cosmic_id"]
        l = df.columns[7]
        line_df = df
        
        line_df["grouped_p_var"] = [var]*len(line_df)
        line_df["var_gene_name"] = [gene_name]*len(line_df)
        line_df["dr_ctype"] = [ctype]*len(line_df)
        return line_df
    # if more than one cell line is included
    else:
        df = df.drop([c for c in df.columns if "celllines" in c], axis=1)
        unique_cols = df.columns[:7].tolist()
        # rshape the df so that the general info fields (unique cols) are repeated for each cell line
        # stack the cell-line specific fields (col_subset)
        for l in lines:
            col_subset = unique_cols + [c for c in df.columns if l in c]
            line_df = df[col_subset]
            line_df.columns=colnames + ["cosmic_id"]
            line_df["cell_line"] = [l]*len(line_df)
            gene_name = filename.split("/")[-1].split()[0]
            var = " | ".join([s.rstrip("_screenRes.tsv").rstrip(" ").strip(f"{gene_name} _ ***") for s in filename.              split("/")[-1].split("|") if "p." in s])
            ctype = filename.split("/")[-2]
            line_df["grouped_p_var"] = [var]*len(line_df)
            line_df["var_gene_name"] = [gene_name]*len(line_df)
            line_df["dr_ctype"] = [ctype]*len(line_df)
            arr.append(line_df)
        return pd.concat(arr)


# read all the dependency hits drug response analysis results
# the drug response analysis computes a tissue-specific response rank ratio for
# each variant (or group of variants on same gene), similar to the dependency analysis
# the percentile & rank ratio indicate how that cell line's response to the drug
# compares to other cell lines of the same lineage/ctype
filenames=snakemake.input 
dfs = [reshape_df(f) for f in filenames]
drug_response_analysis_data = pd.concat(dfs)
drug_response_analysis_data["cosmic_id"] = drug_response_analysis_data["cosmic_id"].astype(int).astype(str)
# explode grouped variants (keep grouped ids for reference)
drug_response_analysis_data["hgvs_protein"] = drug_response_analysis_data.grouped_p_var.apply(
    lambda x: x.split(" | "))
drug_response_analysis_data = drug_response_analysis_data.explode(
    'hgvs_protein').reset_index(drop=True)
drug_response_analysis_data["hgvs_protein"] = drug_response_analysis_data.apply(lambda x:x["var_gene_name"] + ":" + x["hgvs_protein"], axis=1)

drug_response_analysis_data.to_csv(
    snakemake.output.var_ctype_response_rank_tab, sep="\t", index=None)

