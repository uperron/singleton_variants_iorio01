# ---
# jupyter:
#   jupytext:
#     formats: ipynb,py
#     text_representation:
#       extension: .py
#       format_name: light
#       format_version: '1.5'
#       jupytext_version: 1.6.0
#   kernelspec:
#     display_name: singleton_variants
#     language: python
#     name: singleton_variants
# ---

# # Introduction
# State notebook purpose here

# ### Imports
# Import libraries and write settings here.

# +
# Data manipulation
import pandas as pd
import numpy as np
from glob import glob
import math

# Options for pandas
# No warnings about setting value on copy of slice
pd.options.mode.chained_assignment = None
pd.options.display.max_columns = 600
pd.options.display.max_rows = 30

# Display all cell outputs
from IPython.core.interactiveshell import InteractiveShell
InteractiveShell.ast_node_interactivity = 'all'

from IPython import get_ipython
ipython = get_ipython()

# autoreload extension
if 'autoreload' not in ipython.extension_manager.loaded:
    %load_ext autoreload

# %autoreload 2

# Visualizations
import matplotlib.pyplot as plt
# Set default font size
plt.rcParams['font.size'] = 24
import seaborn as sb
# Set default font size
sb.set(font_scale = 1)
custom_style = {'axes.labelcolor': 'black',
                'xtick.color': 'black',
                'ytick.color': 'black'}
sb.set_style("white", rc=custom_style)


# Interactive Visualizations
# import plotly.plotly as py
# import plotly.graph_objs as go
# from plotly.offline import iplot, init_notebook_mode
# init_notebook_mode(connected=True)

# import cufflinks as cf
# cf.go_offline(connected=True)
# icf.set_config_file(theme='white')
# -

# # Analysis/Modeling
# Do work here

# +
filenames = glob('data/Right_Pip_Results1/*.tsv')
dependency_df = pd.concat(
    [pd.read_csv(f, sep="\t", header=0) for f in filenames])
filename = "data/TableS2C.csv"
# load full dataset of variants (rare & non-rare) 
variant_data = pd.read_csv(filename, sep=",", skiprows=20)
variant_data["hgvs"] = variant_data[['Transcript', 'cDNA']].agg(
    ':'.join, axis=1).values

# expand variants (e.g. var1 | var 2), one per row
gb = dependency_df[["GENE", "var","ctype", 'medFitEff', 'rank_ratio']].groupby(["GENE", 
                    "ctype", 'medFitEff', 'rank_ratio'])
arr = []
for name, group in gb:
    GENE,ctype,medFitEff,rank_ratio = name
    l = group["var"].astype(str).apply(
        lambda x: x.split(" | ")).values[0]
    for e in l:
        arr.append([GENE,ctype,medFitEff,rank_ratio,e])
dependency_df = pd.DataFrame(arr, columns=["GENE", "ctype", 'medFitEff', 'rank_ratio', "var"])
dependency_df = pd.merge(variant_data, dependency_df, left_on=["Gene", "AA"],
                         right_on=["GENE", "var"], how="right")

dependency_df.COSMIC_ID = dependency_df.COSMIC_ID.astype(int).astype(str)

# add bool col for gene essentiality filter
medFitEff_max = -.5
rank_ratio_max = 1.2
dependency_df["essentiality_annot"] = (dependency_df["medFitEff"] < medFitEff_max) &\
    (dependency_df["rank_ratio"] < rank_ratio_max)

# load VEP data from ensembl VEP
vep_data = pd.read_csv("data/significant_vep_data_hgvs_clean.tsv",
                       sep="\t", index_col=0)
dependency_vep_df = pd.merge(vep_data, dependency_df,
                             left_on="id",
                             right_on="hgvs",
                             how="right")

# add bool col for probable deleterious variants
impact_keep = ["MODERATE"]
poly_keep = ["probably_damaging", "possibly_damaging", "nan"]
sift_keep = ["deleterious", "nan"]
dependency_vep_df["vep_annot"] = dependency_vep_df.eval("(impact in @impact_keep &\
polyphen_prediction in @poly_keep \
& sift_prediction in @sift_keep) | (impact == 'HIGH')")
dependency_vep_df = dependency_vep_df[["var", "hgvs", "COSMIC_ID", "GENE", 'Transcript', "ctype",
                                       'essentiality_annot', 'vep_annot',
                                       'impact', 'consequence_terms',
                                       'cadd_raw', 'cadd_phred',
                                       'polyphen_prediction', 'sift_prediction',
                                       'medFitEff', 'rank_ratio']].drop_duplicates()

# add drug-target info from Goncalves et al 2020
target_data = pd.read_csv("data/full_gene-target_annot.tsv", sep="\t")
dependency_vep_target_df = pd.merge(target_data, dependency_vep_df,
                                    left_on="target_ENST_id",
                                    right_on="Transcript",
                                    how="right")
dependency_vep_target_df = dependency_vep_target_df.rename(
    columns={"Name": "DRUG_NAME_target"})
# add bool col for target annotation (aka is there a drug that targets the variant's gene?)
dependency_vep_target_df["drug-gene_annot"] = dependency_vep_target_df.DRUG_NAME_target.notna()

#load response data(IC50) from GDSC1 & GDSC2
GDSC_response_data = pd.concat([pd.read_csv("data/GDSC1_fitted_dose_response_25Feb20.csv"),
                                pd.read_csv("data/GDSC2_fitted_dose_response_25Feb20.csv")]).drop_duplicates()
GDSC_response_data = GDSC_response_data.rename(
    columns={"DRUG_NAME": "DRUG_NAME_response"})

GDSC_response_data.COSMIC_ID = GDSC_response_data.COSMIC_ID.astype(int).astype(str)

# add drug response info
dependency_vep_target_response_df = pd.merge(dependency_vep_target_df, GDSC_response_data,
                                             left_on=["COSMIC_ID",
                                                      "DRUG_NAME_target"],
                                             right_on=["COSMIC_ID",
                                                       "DRUG_NAME_response"],
                                             how="left").drop_duplicates()
# check if cell_line;drug in GDSC
dependency_vep_target_response_df["drug_cell_in_GDSC"] = dependency_vep_target_response_df.DRUG_NAME_response.notna()
dependency_vep_target_response_df["IC50"] = dependency_vep_target_response_df["LN_IC50"].astype(float).\
    apply(lambda x: math.exp(x))
# check if IC50<max_c
dependency_vep_target_response_df["IC50<MAX_CONC"] = dependency_vep_target_response_df.IC50 < \
dependency_vep_target_response_df.MAX_CONC

# pick the best (<IC50) drug for each variant 
all_filters_df = dependency_vep_target_response_df[(dependency_vep_target_response_df.essentiality_annot == True) & \
                                 (dependency_vep_target_response_df.vep_annot == True) & \
                                 (dependency_vep_target_response_df["drug-gene_annot"] == True) & \
                                 (dependency_vep_target_response_df.drug_cell_in_GDSC == True) & \
                                 (dependency_vep_target_response_df["IC50<MAX_CONC"] == True)].sort_values('IC50').\
groupby(["COSMIC_ID", 'hgvs']).head(1).drop_duplicates()

dependency_vep_target_response_df["best_drug_for_var"] = \
[True if x in all_filters_df.index else False for x in dependency_vep_target_response_df.index]

dependency_vep_target_response_df
# -

dependency_vep_target_response_df = dependency_vep_target_response_df[["var", "hgvs", "COSMIC_ID", "CELL_LINE_NAME",
                                                                       "GENE", "ctype", "DRUG_NAME_response", "Transcript",
                                                                       'essentiality_annot',
                                                                       'vep_annot', "drug-gene_annot", 
                                                                       "drug_cell_in_GDSC",
                                                                       "IC50<MAX_CONC", "best_drug_for_var",
                                                                       'impact', 'consequence_terms',
                                                                       'cadd_phred', 'polyphen_prediction', 
                                                                       'sift_prediction', 'medFitEff', 'rank_ratio',
                                                                       "DRUG_NAME_target", 
                                                                       "LN_IC50","IC50","MAX_CONC"]]
dependency_vep_target_response_df.sort_values('IC50').\
to_csv("data/dependency_vep_target_response_ALL_PIPright_VARS.tsv", sep="\t", index=None)

# # Results
# Show graphs and stats here

leads_df = dependency_vep_target_response_df[(dependency_vep_target_response_df.essentiality_annot == True) & \
                                 (dependency_vep_target_response_df.vep_annot == True) & \
                                 (dependency_vep_target_response_df["drug-gene_annot"] == True) & \
                                 (dependency_vep_target_response_df.drug_cell_in_GDSC == True) & \
                                 (dependency_vep_target_response_df["IC50<MAX_CONC"] == True) &\
                                (dependency_vep_target_response_df.best_drug_for_var == True)].\
sort_values('IC50')
leads_df.to_csv("data/dependency_vep_target_response_ALL_PIPright_VARS_bestdrugCELLVAR.tsv", sep="\t")
leads_df

# +
# best drug for each of our variant
import matplotlib.ticker as mtick
leads_df["IC50_ratio"] = leads_df.IC50 / leads_df.MAX_CONC

fig, ax = plt.subplots(figsize=(8, 5))
ax = leads_df["IC50_ratio"].plot.hist(cumulative=True)
ax.set_xlabel("IC50 / max concentration", fontsize=14);
ax.set_ylabel("Cumulative variant - drug pair (%)", fontsize=14);
ax.yaxis.set_major_formatter(mtick.PercentFormatter(leads_df.shape[0]))

fig.savefig(f"figures/bestdrug-var_IC50maxC_GDSC.pdf", format="pdf", dpi=fig.dpi)

# +
#best_drug_df = pd.read_csv("data/significant_hgvs_GDSC12_IC50_bestdrugs.tsv", sep="\t", index_col=None)
#best_drug_df
# -

# # Conclusions and Next Steps
# Summarize findings here


