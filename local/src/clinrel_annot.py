#!/usr/bin/env python
from pprint import pprint
from bravado.client import SwaggerClient
import pandas as pd
import numpy as np

# Options for pandas
# No warnings about setting value on copy of slice
pd.options.mode.chained_assignment = None
pd.options.display.max_columns = 600
pd.options.display.max_rows = 30

# setup connection to genome nexus API
gn = SwaggerClient.from_url('https://www.genomenexus.org/v2/api-docs',
                            config={"validate_requests": False,
                                    "validate_responses": False,
                                    "validate_swagger_spec": False})
# to split input into chunks
enst_id = snakemake.wildcards.enst_id
variant_data = pd.read_csv(snakemake.input[0], sep="\t", header=0)
variant_data = variant_data[variant_data.Transcript == enst_id]

tcga_all_prj_mut_df = pd.read_csv(snakemake.input[1],  sep="\t", header=0)

# get cancerhotspots annot from genome nexus
# is var a statistically significant mut hotspot?
# see https://www.cancerhotspots.org      DOI: 10.1158/2159-8290.CD-17-0321


def get_hotspot_annot(var):
    c, s, e, r, v = var.split(",")
    alt_var = ",".join([c, s, e, v, r])
    try:
        # see: https://github.com/mskcc/cbsp-hackathon/blob/master/0-introduction/cbsp_hackathon.ipynb
        result = gn.annotation_controller.fetchVariantAnnotationByGenomicLocationGET(
            genomicLocation=var,
            # adds extra annotation resources, not included in default response:
            fields='hotspots annotation_summary'.split()
        ).result()
        print(f"{var} annotated.")
        return result.hotspots
    except:
        try:
            # try to flip ref/var nucleotide
            result = gn.annotation_controller.fetchVariantAnnotationByGenomicLocationGET(
                genomicLocation=alt_var,
                fields='hotspots mutation_assessor annotation_summary'.split()
            ).result()
            print(f"{var} annotated (flipped).")
            return result.hotspots
        except:
            print(f"{var} not found.")
            return None


# add hotspot annotation
variant_data["hotspot_annot"] = variant_data.query_str.apply(get_hotspot_annot)

# add bool col for fitering


def is_hotspot(x):
    if x:
        return sum([len(a) for a in x.annotation]) > 0
    else:
        return False


variant_data["is_hotspot"] = variant_data["hotspot_annot"].apply(is_hotspot)
variant_data = variant_data.drop("hotspot_annot", axis=1)

tot_patients_pan = tcga_all_prj_mut_df.uniquePatientKey.unique().shape[0]
tot_patients_spec = tcga_all_prj_mut_df.groupby(
    "TCGA_PRJ").uniquePatientKey.nunique()

# is var in tcga pan cancer?
# in how many patients pan cancer / what freq?
# in which prjs?
# is var in prj that corresponds to cell ctype and dep analysis ctype?
# if so in how many patients / what freq?


def query_tcga_mut(var, cell_tcga_prj):
    out = [var, cell_tcga_prj, False, 0, 0, "", {}, False, 0, 0]
    try:
        c, s, e, r, v = str(var).split(",")
    except ValueError:
        return out
    result = tcga_all_prj_mut_df.query(
        'chr==@c & startPosition==@s & endPosition==@e &\
        referenceAllele==@r & variantAllele ==@v')
    # try to flip reference & variant (other strand?)
    if len(result) == 0:
        result = tcga_all_prj_mut_df.query(
            'chr==@c & startPosition==@s & endPosition==@e &\
        referenceAllele==@v & variantAllele ==@r')
    # var is not in tcga
    if len(result) == 0:
        return out
    # compute output
    else:
        isin_tcga_pan = True
        n_patients_pan = result.uniquePatientKey.unique().shape[0]
        freq_pan = n_patients_pan / tot_patients_pan
        prjs = ",".join(result.TCGA_PRJ.unique())
        freq_all_prj = {}
        for prj in result.TCGA_PRJ.unique().tolist():
            n_patients_spec = result[result.TCGA_PRJ ==
                                     prj].uniquePatientKey.unique().shape[0]
            freq_all_prj[prj] = n_patients_spec / tot_patients_spec[prj]
        is_ctype_spec = cell_tcga_prj in list(freq_all_prj.keys())
        if is_ctype_spec:
            freq_spec = freq_all_prj[cell_tcga_prj]
            n_patients_spec = result[result.TCGA_PRJ ==
                                     cell_tcga_prj].uniquePatientKey.unique().shape[0]
        else:
            freq_spec = 0
            n_patients_spec = 0
        out = [var, cell_tcga_prj, isin_tcga_pan, n_patients_pan, freq_pan, prjs, freq_all_prj,
               is_ctype_spec, n_patients_spec, freq_spec]
    return out


arr = []
for var, cell_tcga_prj in zip(variant_data['query_str'].values,
                              variant_data['CELL_TCGA_PRJ'].values):
    arr.append(query_tcga_mut(var, cell_tcga_prj))
tcga_annot = pd.DataFrame(arr, columns=["query_str", "CELL_TCGA_PRJ", "isin_tcga_pan", "n_patients_pan", "var_freq_pan",
                                        "prjs", "freq_all_prj", "isin_ctype_spec_prj", "n_patients_var_spec", "var_freq_spec"])

out_df = pd.merge(variant_data, tcga_annot, on=[
                  "query_str", "CELL_TCGA_PRJ"], how="left")
out_df = out_df[["chromosome",
                 "start",
                 "tr_hgvs",
                 "referenceAllele",
                 "variantAllele",
                 "hgvs",
                 "CELL_TCGA_PRJ",
                 "end",
                 "query_str",
                 "Transcript",
                 "is_hotspot",
                 "isin_tcga_pan",
                 "n_patients_pan",
                 "var_freq_pan",
                 "prjs",
                 "freq_all_prj",
                 "isin_ctype_spec_prj",
                 "n_patients_var_spec",
                 "var_freq_spec"]]
out_df.to_csv(snakemake.output[0], sep="\t", index=None)
